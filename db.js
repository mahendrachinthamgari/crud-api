const Pool = require('pg').Pool;
const dotenv = require('dotenv')

dotenv.config()

const pool = new Pool({
    user: process.env.USER_NAME,
    host: process.env.HOST,
    database: process.env.DATABASE,
    password: process.env.PASSWORD,
});

module.exports = pool;