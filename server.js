const express = require('express');
const studentRoutes = require('./src/student/routes');

const port = process.env.PORT;

const app = express();

app.use(express.json());

app.get("/", (req, res) => {
    res.send ("Hello World");
});

app.use('/api/v1/students', studentRoutes);

app.listen(port, () => {
    console.log(`Listening at port ${port}`);
});