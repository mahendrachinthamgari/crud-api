const pool = require('../../db');
const queries = require('./queries');

const getStudents = (req, res) => {
    pool.query(queries.getStudents, (error, results) => {
        if (error) {
            throw error;
        } else {
            res.status(200).json(results.rows);
        }
    });
}

const getStudentsById = (req, res) => {
    const id = parseInt(req.params.id);
    pool.query(queries.getStudentsById, [id], (error, results) => {
        if (error) {
            throw error;
        } else {
            res.status(200).json(results.rows)
        }
    });
}

const addStudent = (req, res) => {
    const { name, email, age, dob } = req.body;

    pool.query(queries.checkEmailExists, [email], (error, results) => {
        if (results.rows.length) {
            res.send("Email already exists.");
        } else {

            pool.query(queries.addStudent, [name, email, age, dob], (error, results) => {
                if (error) {
                    throw error;
                } else {
                    res.status(201).send("Student Created Successfully");
                }
            });
        }

    });
}

const removeStudent = (req, res) => {
    const id = parseInt(req.params.id);

    pool.query(queries.getStudentsById, [id], (error, results) => {
        const noStudentFound = !results.rows.length;
        if (noStudentFound) {
            res.send("Student does not exist in the database");
        } else {

            pool.query(queries.removeStudent, [id], (error, results) => {
                if (error) {
                    throw error;
                } else {
                    res.status(200).send("Student removed successfully.");
                }
            });
        }
    });
}

const updateStudent = (req, res) => {
    const id = parseInt(req.params.id);
    const { name, email, age, dob } = req.body;

    pool.query(queries.getStudentsById, [id], (error, results) => {
        const noStudentFound = !results.rows.length;
        if (noStudentFound) {
            res.send("Student does not exist in the database");
        } else {

            pool.query(queries.updateStudent, [id, name, email, age, dob], (error, results) => {
                if (error) {
                    throw error;
                } else {
                    res.status(200).send("Student updated successfully")
                }
            });
        }
    });
}


module.exports = {
    getStudents,
    getStudentsById,
    addStudent,
    removeStudent,
    updateStudent,
}